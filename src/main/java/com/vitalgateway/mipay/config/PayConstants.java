package com.vitalgateway.mipay.config;

public class PayConstants {

    public static final String GET = "get";

    public static final String POST = "post";

    /**
     * Date-time string format
     */
    public static final String DATE_FORMAT_PATTERN = "yyyyMMddHHmmssSSS";
    /**
     * Signature string
     */
    public static final String FIELD_SIGN = "Signature";
    /**
     * Merchant ID code in string form
     */
    public static final String MCH_ID = "MrchtID";
    /**
     * DistributorId
     */
    public static final String DISTRIBUTOR_ID = "DistributorId";
    /**
     * Random string for security purpose
     */
    public static final String NonceStr = "NonceStr";
    /**
     * Quick Pay - Merchant scanning payer's payment code
     */
    public static final String MICROPAY_URL_SUFFIX = "https://testapi.mi-pay.com.au/payway/payment/micropay";
    /**
     * Native Pay - Payer scanning merchant's receiving QR code
     */
    public static final String SCANPAY_URL_SUFFIX = "https://testapi.mi-pay.com.au/payway/payment/scanpay";
    /**
     * Official Account Payment - also known as JSAPI
     */
    public static final String JSPAY_URL_SUFFIX = "https://mi-pay.com.au/wxjsapi/showpayinfo/underWxPay/testapi.mi-pay.com.au";


    public static final String UNIONPAY_H5_URL_SUFFIX = "https://testapi.mi-pay.com.au/unionPayH5/testapi.mi-pay.com.au";
    /**
     * Payment order status query
     */
    public static final String QUERY_URL_SUFFIX = "https://testapi.mi-pay.com.au/payway/payment/query";

    public static final String REFUNDQUERY_URL_SUFFIX = "https://testapi.mi-pay.com.au/payway/pay/refundQuery";
    /**
     * Payment refund
     */
    public static final String REFUND_URL_SUFFIX = "https://testapi.mi-pay.com.au/payway/pay/refund";
    /**
     * Payment Query rate
     */
    public static final String RATE_URL_SUFFIX = "https://testapi.mi-pay.com.au/payway/payment/rate";
    /**
     * Payment app
     */
    public static final String APPPAY_URL_SUFFIX = "https://testapi.mi-pay.com.au/payway/payment/apppay";
    /**
     * Batch Query
     */
    public static final String BATCH_QUERY = "https://testapi.mi-pay.com.au/payway/payment/batchQuery";

    public static final String SMPPAY_URL_SUFFIX = "https://testapi.mi-pay.com.au/payway/payment/smppay";

    /**
     * alipay app
     */
    public static final String ALIAPP_URL_SUFFIX = "https://testapi.mi-pay.com.au/payway/payment/app";

    /**
     * alipay wap
     */
    public static final String ALIWAP_URL_SUFFIX = "https://testapi.mi-pay.com.au/payway/payment/wap";

    /**
     * alipay website
     */
    public static final String ALIWEBSITE_URL_SUFFIX = "https://testapi.mi-pay.com.au/payway/payment/website";
}

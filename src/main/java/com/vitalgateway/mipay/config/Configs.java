package com.vitalgateway.mipay.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configs {

    private static String MrchtID;
    private static String SignKey;
    private static String DistributorId;
    private static String DistributorSignKey;

    static {
        Properties prop = new Properties();
        try {
            InputStream in = Configs.class.getResourceAsStream("/config.properties");
            if (in == null) {
                System.out.println("Cannot find file config.properties!");
            } else {
                prop.load(in);
                setMrchtID(prop.getProperty("mrchant_id"));
                setSignKey(prop.getProperty("sign_key"));
                setDistributorId(prop.getProperty("distributor_id"));
                setDistributorSignKey(prop.getProperty("distributor_sign_key"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getMrchtID() {
        return MrchtID;
    }

    public static void setMrchtID(String mrchtID) {
        MrchtID = mrchtID;
    }

    public static String getSignKey() {
        return SignKey;
    }

    public static void setSignKey(String signKey) {
        SignKey = signKey;
    }

    public static String getDistributorId() {
        return DistributorId;
    }

    public static void setDistributorId(String distributorId) {
        DistributorId = distributorId;
    }

    public static String getDistributorSignKey() {
        return DistributorSignKey;
    }

    public static void setDistributorSignKey(String distributorSignKey) {
        DistributorSignKey = distributorSignKey;
    }
}


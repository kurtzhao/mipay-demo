package com.vitalgateway.mipay;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.vitalgateway.mipay.config.PayConstants;
import com.vitalgateway.mipay.utils.HttpRequest;
import com.vitalgateway.mipay.utils.PayUtils;

import java.util.HashMap;

public class MainEntry {

    public static void main(String[] args) throws Exception {
//		 query();
//		 refundQuery();
//		 JsPay();
//		 JsPayHK();
//		 scanpay();
//		 scanpayHK();
//		 micropay();
//		 refund();
//		 rate();
//		 apppay();
//		 batchQuery();
//		 batchQueryByDistributor();
//       smppay();
//		 unionpayH5Pay();
//		 alipayApp();
//		 alipayWap();
//		 alipayWebSite();
    }

    /**
     * Payment order query
     * Query with the order number returned by the payment request call
     *
     * @throws Exception
     */
    public static void query() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        // replace below payment order number with the one returned by payment call
        map.put("MrchtOrderNo", "20190326160658000004");
        // initiate query request
        String request = hr.Request(PayConstants.QUERY_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
    }

    public static void refundQuery() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        // replace below payment order number with the one returned by payment call
        map.put("RefundOrderNo", "201811121825422106809046814");
        // initiate query request
        String request = hr.Request(PayConstants.REFUNDQUERY_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
    }

    /**
     * Official Account Payment
     * After the call, open the returned HTTP link in WeChat client end
     *
     * @throws Exception
     */
    public static void JsPay() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("TotalAmt", "2");
        map.put("Product", "ABC");
        map.put("ExtraInfo", "testACC");
        map.put("NotifyURL", "https://linlinjava.free.idcfengye.com/wx/order/pay-notify");
        // initiate payment request
        String request = hr.Request(PayConstants.JSPAY_URL_SUFFIX, map, PayConstants.GET);
        System.out.println(request);
    }

    /**
     * Official Account Payment HK
     * After the call, open the returned HTTP link in WeChat client end
     *
     * @throws Exception
     */
    public static void JsPayHK() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("TotalAmt", "2");
        map.put("Product", "DummyItem");
        map.put("NotifyURL", "https://testadmin.mi-pay.com.au/payway/notify");
        map.put("ReturnPage", "https://www.mi-pay.com.au");
        //0000 WeChat Pay (cross border), 0001 WeChat Pay HK, 0002 Alipay (cross border), 0003 Alipay HK
        map.put("Channel", "0001");
        // initiate payment request
        String request = hr.Request(PayConstants.JSPAY_URL_SUFFIX, map, PayConstants.GET);
        System.out.println(request);
    }

    /**
     * Native Pay - Payer scanning merchant's receiving QR code
     * After the call, convert the returned CodeURL into a QR code;
     * or decode the returned base64 CodeImg and display the QR code picture
     *
     * @throws Exception
     */
    public static void scanpay() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("TotalAmt", "101");
        map.put("Product", "DummyItem");
        //0000 WeChat Pay (cross border), 0001 WeChat Pay HK, 0002 Alipay (cross border), 0003 Alipay HK
        map.put("Channel", "0000");
        //map.put("Local", "CN");
        map.put("NotifyURL", "https://testadmin.mi-pay.com.au/payway/notify");
        // initiate payment request
        String request = hr.Request(PayConstants.SCANPAY_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
    }

    /**
     * Native Pay HK - Payer scanning merchant's receiving QR code
     * After the call, convert the returned CodeURL into a QR code;
     * or decode the returned base64 CodeImg and display the QR code picture
     *
     * @throws Exception
     */
    public static void scanpayHK() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("TotalAmt", "2");
        map.put("Product", "order test");
        map.put("ExtraInfo", "order test");
        //0000 WeChat Pay (cross border), 0001 WeChat Pay HK, 0002 Alipay (cross border), 0003 Alipay HK
        map.put("Channel", "0003");
        map.put("NotifyURL", "https://testadmin.mi-pay.com.au/payway/notify");
        // initiate payment request
        String request = hr.Request(PayConstants.SCANPAY_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
    }

    /**
     * Quick Pay - Merchant scanning payer's payment code
     *
     * @throws Exception
     */
    public static void micropay() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("TotalAmt", "2");
        map.put("Product", "order test");
        // Get authorization code by scanning payer's payment code which is simulated below
        map.put("AuthCode", "135541810038397229");
        //map.put("Channel", "0002");
        // initiate payment request
        String request = hr.Request(PayConstants.MICROPAY_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
    }

    /**
     * Refund - use the payment returned order number to initiate refund
     *
     * @throws Exception
     */
    public static void refund() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        // replace below payment order number with the one returned by payment call
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("PaidAmt", "101");
        map.put("RefundAmt", "101");
        map.put("RefundOrderNo", "201904191145046295404568380");
        // initiate refund request
        String request = hr.Request(PayConstants.REFUND_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
    }

    /**
     * Rate Query
     *
     * @throws Exception
     */
    public static void rate() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        // Currency	code in	ISO	4217 standard
        map.put("Currency", "HKD");
        // initiate refund request
        String request = hr.Request(PayConstants.RATE_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
    }

    /**
     * APP Payment
     *
     * @throws Exception
     */
    public static void apppay() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        // Currency	code in	ISO	4217 standard
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("TotalAmt", "2");
        map.put("Product", "DummyItem");
        map.put("NotifyURL", "https://testadmin.mi-pay.com.au/payway/notify");
        // initiate refund request
        String request = hr.Request(PayConstants.APPPAY_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
    }

    /**
     * Mini program payment
     *
     * @throws Exception
     */
    public static void smppay() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        // Currency	code in	ISO	4217 standard
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("TotalAmt", "2");
        map.put("Product", "DummyItem");
        map.put("OpenId", "o13jy1UjGAL_R6YX-XXfuSbB-SlA");
        map.put("NotifyURL", "https://testadmin.mi-pay.com.au/payway/notify");
        // initiate refund request
        String request = hr.Request(PayConstants.SMPPAY_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
    }

    /**
     * Merchant's query of order status in batch
     *
     * @throws Exception
     */
    public static void batchQuery() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        // Currency	code in	ISO	4217 standard
        map.put("StartDate", "2019-03-11");
        map.put("EndDate", "2019-03-12");
        // initiate refund request
        String request = hr.Request(PayConstants.BATCH_QUERY, map, PayConstants.POST);
        System.out.println(request);
    }

    /**
     * Agent's query of order status in batch
     *
     * @throws Exception
     */
    public static void batchQueryByDistributor() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        // Currency	code in	ISO	4217 standard
        map.put("StartDate", "2018-10-01");
        map.put("EndDate", "2018-10-07");
        // initiate refund request
        String request = hr.RequestByDistributor(PayConstants.BATCH_QUERY, map);
        System.out.println(request);
    }

    /**
     * unionpay H5Pay
     *
     * @throws Exception
     */
    public static void unionpayH5Pay() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("TotalAmt", "2");
        map.put("Product", "order test");
        map.put("NotifyURL", "https://testadmin.mi-pay.com.au/payway/notify");
        // initiate payment request
        String request = hr.Request(PayConstants.UNIONPAY_H5_URL_SUFFIX, map, PayConstants.GET);
        System.out.println(request);
    }

    /**
     * alipay apppay
     *
     * @throws Exception
     */
    public static void alipayApp() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        // Currency	code in	ISO	4217 standard
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("TotalAmt", "2");
        map.put("Product", "alipay apppay test");
        map.put("Currency", "AUD");
        map.put("NotifyURL", "https://testadmin.mi-pay.com.au/payway/notify");
        // initiate refund request
        String request = hr.Request(PayConstants.ALIAPP_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
    }

    /**
     * alipayWap
     *
     * @throws Exception
     */
    public static void alipayWap() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        // Currency	code in	ISO	4217 standard
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("TotalAmt", "2");
        map.put("Product", "alipay alipayWap test");
        map.put("Currency", "HKD");
        map.put("NotifyURL", "https://testadmin.mi-pay.com.au/payway/notify");
        // initiate refund request
        String request = hr.Request(PayConstants.ALIWAP_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
    }

    /**
     * alipayWebSite
     *
     * @throws Exception
     */
    public static void alipayWebSite() throws Exception {
        HttpRequest hr = new HttpRequest();
        HashMap<String, String> map = new HashMap<>();
        // Currency	code in	ISO	4217 standard
        map.put("MrchtOrderNo", PayUtils.getUniOrder());
        map.put("TotalAmt", "2");
        map.put("Product", "alipay alipayWebSite test");
        map.put("Currency", "HKD");
        map.put("NotifyURL", "https://testadmin.mi-pay.com.au/payway/notify");
        // initiate refund request
        String request = hr.Request(PayConstants.ALIWEBSITE_URL_SUFFIX, map, PayConstants.POST);
        System.out.println(request);
        JSONObject json = JSON.parseObject(request);
        System.out.println(json.get("Content"));
    }
}

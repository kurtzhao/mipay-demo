package com.vitalgateway.mipay.utils;

import com.vitalgateway.mipay.config.PayConstants;

import java.security.MessageDigest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

public class PayUtils {

    static Logger logger = Logger.getGlobal();

    /**
     * MD5 encryption
     *
     * @param data
     * @return
     */
    public static String MD5(String data) {
        StringBuilder sb = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(data.getBytes("UTF-8"));
            sb = new StringBuilder();
            for (byte item : array) {
                sb.append(Integer.toHexString((item & 0xFF) | 0x100).substring(1, 3));
            }
        } catch (Exception e) {
            return null;
        }

        return sb.toString().toUpperCase();
    }

    /**
     * Data signing
     *
     * @param data
     * @param key
     * @return
     */
    public static String generateSignature(final Map<String, String> data, String key) {
        logger.info("generateSignature :" + data.toString());
        Set<String> keySet = data.keySet();
        String[] keyArray = keySet.toArray(new String[keySet.size()]);
        Arrays.sort(keyArray);
        StringBuilder sb = new StringBuilder();
        for (String k : keyArray) {
            if (k == null || data.get(k) == null || "".equals(k)) {
                continue;
            }
            if (data.get(k).toString().trim().length() > 0) // not involved in signing if value is empty
                sb.append(k).append("=").append(data.get(k)).append("&");
        }
        sb.append("key=").append(key);
        System.out.println(sb.toString());
        return MD5(sb.toString());
    }

    /**
     * Data signing
     *
     * @param data
     * @param key
     * @return
     */
    public static String generateSignatures(final Map<String, Object> data, String key) {
        logger.info("generateSignature :" + data.toString());
        Set<String> keySet = data.keySet();
        String[] keyArray = keySet.toArray(new String[keySet.size()]);
        Arrays.sort(keyArray);
        StringBuilder sb = new StringBuilder();
        for (String k : keyArray) {
            if ("".equals(k)) {
                continue;
            }
            if (data.get(k).toString().trim().length() > 0) // not involved in signing if value is empty
                sb.append(k).append("=").append(data.get(k)).append("&");
        }
        sb.append("key=").append(key);
        System.out.println(sb.toString());
        return MD5(sb.toString());
    }

    /**
     * Generate simulated order number
     *
     * @return
     */
    public static String getUniOrder() {
        LocalDateTime localDateTime = LocalDateTime.now();
        String no = localDateTime.format(DateTimeFormatter.ofPattern(PayConstants.DATE_FORMAT_PATTERN));
        for (int i = 0; i < 10; i++) {
            no += String.valueOf((int) (Math.random() * (10)));
        }
        return no;
    }

    /**
     * Generate random string for NonceStr
     *
     * @return
     */
    public static String generateNonceStr() {
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0, 32);
    }

}

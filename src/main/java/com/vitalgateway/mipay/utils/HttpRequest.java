package com.vitalgateway.mipay.utils;

import com.alibaba.fastjson.JSON;
import com.vitalgateway.mipay.config.Configs;
import com.vitalgateway.mipay.config.PayConstants;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import java.util.Base64;
import java.util.Map;
import java.util.logging.Logger;

public class HttpRequest {

    static Logger logger = Logger.getGlobal();

    public String sendPost(String url, String data) throws Exception {
        BasicHttpClientConnectionManager connManager = new BasicHttpClientConnectionManager(
                RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.getSocketFactory())
                        .register("https", SSLConnectionSocketFactory.getSocketFactory())
                        .build(), null, null, null);
        CloseableHttpClient httpClient = HttpClientBuilder.create().setConnectionManager(connManager).build();
        HttpPost httpPost = new HttpPost(url);
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(80000).setConnectTimeout(8000).build();
        httpPost.setHeader("Content-Type", "application/json");
        httpPost.setConfig(requestConfig);
        StringEntity postEntity = new StringEntity(data, "UTF-8");
        httpPost.setEntity(postEntity);
        HttpResponse httpResponse = httpClient.execute(httpPost);
        HttpEntity httpEntity = httpResponse.getEntity();
        return EntityUtils.toString(httpEntity, "UTF-8");
    }

    public String sendGet(String url) throws Exception {
        BasicHttpClientConnectionManager connManager = new BasicHttpClientConnectionManager(
                RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.getSocketFactory())
                        .register("https", SSLConnectionSocketFactory.getSocketFactory())
                        .build(), null, null, null);
        CloseableHttpClient httpClient = HttpClientBuilder.create().setConnectionManager(connManager).build();
        HttpGet httpGet = new HttpGet(url);
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(8000).setConnectTimeout(8000).build();
        httpGet.setConfig(requestConfig);
        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();
        return EntityUtils.toString(httpEntity, "UTF-8");
    }

    public String Request(String url, Map<String, String> data, String method) throws Exception {
        data.put(PayConstants.MCH_ID, Configs.getMrchtID());
        data.put(PayConstants.NonceStr, PayUtils.generateNonceStr());
        data.put(PayConstants.FIELD_SIGN, PayUtils.generateSignature(data, Configs.getSignKey()));
        String result = null;
        if (PayConstants.GET.equals(method)) {
            logger.info("data :" + data);
            System.out.println(JSON.toJSONString(data));
            url += "/" + Base64.getEncoder().encodeToString(JSON.toJSONString(data).getBytes());
            logger.info("url :" + url);
            return url;
//			result = sendGet(url);
        } else if (PayConstants.POST.equals(method)) {
            logger.info("data :" + data);
            System.out.println(JSON.toJSONString(data));
            result = sendPost(url, JSON.toJSONString(data));
        }
        logger.info("result :" + result);
        return result;
    }

    public String RequestByDistributor(String url, Map<String, String> data) throws Exception {
        data.put(PayConstants.DISTRIBUTOR_ID, Configs.getDistributorId());
        data.put(PayConstants.NonceStr, PayUtils.generateNonceStr());
        data.put(PayConstants.FIELD_SIGN, PayUtils.generateSignature(data, Configs.getDistributorSignKey()));
        String result = null;
        result = sendPost(url, JSON.toJSONString(data));
        logger.info("result :" + result);
        return result;
    }
}
